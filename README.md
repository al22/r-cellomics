# cellomics R package

Utility functions to work with cellomics data.

Right now, only contains a function to read images (.c01 files) into `R`.

## Installation

Installation directly from gitlab is possible, when the package `devtools`
is installed:
```r
library("devtools")
Sys.setenv(PKG_LIBS = "-lz")  ## make sure the linker knows about zlib
install_git("https://gitlab.com/al22/r-cellomics.git")
```