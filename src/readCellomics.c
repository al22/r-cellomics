#include <R.h>
#include <Rdefines.h>
#include "zlib.h"
#include <assert.h>

// taken from (old versions of) EBImage

#define CHUNK 65536
int inflateData(FILE *source, unsigned char **_dat, int *_datsize) {
  int ret;
  unsigned have;
  z_stream strm;
  unsigned char in[CHUNK];
  unsigned char out[CHUNK];
  int datmaxsize;
  unsigned char *dat;
  int datsize;

  // allocate dat with a fixed size ; will be reallocated if necessary
  datmaxsize = CHUNK*2;
  datsize = 0;
  dat = (unsigned char *)malloc(datmaxsize);

  // allocate inflate state
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  strm.avail_in = 0;
  strm.next_in = Z_NULL;
  ret = inflateInit(&strm);
  if (ret != Z_OK) return ret;

  // decompress until deflate stream ends or end of file
  do {
    strm.avail_in = fread(in, 1, CHUNK, source);
    if (ferror(source)) {
      (void)inflateEnd(&strm);
      return Z_ERRNO;
    }
    if (strm.avail_in == 0) break;
    strm.next_in = in;
    // run inflate() on input until output buffer not full
    do {
      strm.avail_out = CHUNK;
      strm.next_out = out;
      ret = inflate(&strm, Z_NO_FLUSH);
      assert(ret != Z_STREAM_ERROR);  // state not clobbered
      switch (ret) {
      case Z_NEED_DICT:
	ret = Z_DATA_ERROR;     // and fall through
      case Z_DATA_ERROR:
      case Z_MEM_ERROR:
	(void)inflateEnd(&strm);
	return ret;
      }
      have = CHUNK - strm.avail_out;
      // reallocate dat if needed
      if (datsize + have >= datmaxsize) {
	datmaxsize = datmaxsize +  CHUNK*2;
	dat = realloc(dat, datmaxsize);
      }
      memcpy(&dat[datsize], out, have);
      datsize += have;
    } while (strm.avail_out == 0);

    // done when inflate() says it's done
  } while (ret != Z_STREAM_END);

  // clean up and return
  (void)inflateEnd(&strm);
  *_datsize = datsize;
  *_dat = dat;
  return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

// File based on http://dev.loci.wisc.edu/trac/software/browser/trunk/components/bio-formats/src/loci/formats/in/CellomicsReader.java
//SEXP readCellomics(const char *filename) {
SEXP readCellomics(SEXP filename) {
  FILE *fin;
  unsigned char *dat, *pdat;
  int datsize;
  SEXP image, dim;
  int i, width, height, nplanes, nbits, compression;
  int nprotect;
  double *dimage;
  int ret;

  // init
  nprotect = 0;

  // open file
  //fin = fopen(filename, "rb");
  fin = fopen(CHAR(STRING_ELT(filename,0)), "rb");
  if (!fin) error("readCellomics: cannot open file");

  // inflate zlib stream
  fseek(fin, 4, SEEK_SET);
  ret = inflateData(fin, &dat, &datsize);
  if (ret!=Z_OK) error("readCellomics: cannot decompress stream");
  fclose(fin);

  // read header
  width = *(int *)(&dat[4]);
  height = *(int *)(&dat[8]);
  nplanes = *(short *)(&dat[12]);
  nbits = *(short *)(&dat[14]);
  compression = *(int *)(&dat[16]);
  if (width*height*nplanes*(nbits/8)+52 > datsize) {
    error("readCellomics: compressed mode is not yed supported");
  }

  // allocate new image
  image = PROTECT(allocVector(REALSXP, width * height * nplanes));
  nprotect++;
  if (nplanes==1) PROTECT(dim=allocVector(INTSXP, 2));
  else PROTECT(dim=allocVector(INTSXP, 3));
  nprotect++;
  INTEGER(dim)[0] = width;
  INTEGER(dim)[1] = height;
  if (nplanes>1) INTEGER(dim)[1] = nplanes;
  SET_DIM (image, dim);

  // copy planes
  dimage = REAL(image);
  pdat = &dat[52];
  if (nbits==8) {
    for (i=0; i<width*height*nplanes; i++) {
      *dimage++ = (*((unsigned char *)pdat))/256.0;
      pdat += sizeof(unsigned char);
    }
  } else if (nbits==16) {
    for (i=0; i<width*height*nplanes; i++) {
      *dimage++ = (*((unsigned short *)pdat))/65536.0;
      pdat += sizeof(unsigned short);
    }
  } else if (nbits==32) {
    for (i=0; i<width*height*nplanes; i++) {
      *dimage++ = (*((unsigned int *)pdat))/4294967296.0;
      pdat += sizeof(unsigned int);
    }
  } else {
    free(dat);
    error("readCellomics: unsupported nbits/pixel mode");
  }

  // free dat
  free(dat);

  UNPROTECT(nprotect);
  return(image);
}
